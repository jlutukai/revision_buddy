class GetHSCodeResponse {
  String status;
  Page page;
  int count;
  List<HsCodes> hsCodes;

  GetHSCodeResponse({this.status, this.page, this.count, this.hsCodes});

  GetHSCodeResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['hs_codes'] != null) {
      hsCodes = new List<HsCodes>();
      json['hs_codes'].forEach((v) {
        hsCodes.add(new HsCodes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.hsCodes != null) {
      data['hs_codes'] = this.hsCodes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class HsCodes {
  int quantity;
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String units;
  String description;
  String code;
  String hsType;
  String id;

  HsCodes(
      {this.quantity,
      this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.units,
      this.description,
      this.code,
      this.hsType,
      this.id});

  HsCodes.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    units = json['units'];
    description = json['description'];
    code = json['code'];
    hsType = json['hs_type'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quantity'] = this.quantity;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['units'] = this.units;
    data['description'] = this.description;
    data['code'] = this.code;
    data['hs_type'] = this.hsType;
    data['id'] = this.id;
    return data;
  }
}
