class GetCategoryResponse {
  String status;
  Page page;
  int count;
  List<Categories> categories;

  GetCategoryResponse({this.status, this.page, this.count, this.categories});

  GetCategoryResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class Categories {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String description;
  HsCode hsCode;
  String code;
  String id;
  String label;
  // Null user;

  Categories({
    this.createdAt,
    this.isActive,
    this.isEditable,
    this.name,
    this.description,
    this.hsCode,
    this.code,
    this.id,
    this.label,
    // this.user
  });

  Categories.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    description = json['description'];
    hsCode =
        json['hs_code'] != null ? new HsCode.fromJson(json['hs_code']) : null;
    code = json['code'];
    id = json['id'];
    label = json['label'];
    // user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['description'] = this.description;
    if (this.hsCode != null) {
      data['hs_code'] = this.hsCode.toJson();
    }
    data['code'] = this.code;
    data['id'] = this.id;
    data['label'] = this.label;
    // data['user'] = this.user;
    return data;
  }
}

class HsCode {
  int quantity;
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String units;
  String description;
  String code;
  String hsType;
  String id;

  HsCode(
      {this.quantity,
      this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.units,
      this.description,
      this.code,
      this.hsType,
      this.id});

  HsCode.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    units = json['units'];
    description = json['description'];
    code = json['code'];
    hsType = json['hs_type'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quantity'] = this.quantity;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['units'] = this.units;
    data['description'] = this.description;
    data['code'] = this.code;
    data['hs_type'] = this.hsType;
    data['id'] = this.id;
    return data;
  }
}
