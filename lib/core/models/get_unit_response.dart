class GetUnitsResponse {
  String status;
  Page page;
  int count;
  List<Units> units;

  GetUnitsResponse({this.status, this.page, this.count, this.units});

  GetUnitsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['units'] != null) {
      units = new List<Units>();
      json['units'].forEach((v) {
        units.add(new Units.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.units != null) {
      data['units'] = this.units.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class Units {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String shortName;
  String id;

  Units(
      {this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.shortName,
      this.id});

  Units.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    shortName = json['short_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['short_name'] = this.shortName;
    data['id'] = this.id;
    return data;
  }
}
