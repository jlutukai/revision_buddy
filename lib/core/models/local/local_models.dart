import 'package:hive/hive.dart';

part 'local_models.g.dart';

@HiveType(typeId: 1)
class CreateAccountBody {
  @HiveField(0)
  String email;
  @HiveField(1)
  String name;
  @HiveField(2)
  String phone;
  @HiveField(3)
  String personalIdentificationNumber;
  @HiveField(4)
  String kraCertificate;
  @HiveField(5)
  String password;
  @HiveField(6)
  String passwordConfirm;
  @HiveField(7)
  bool hasFinishedRegistration = false;
  @HiveField(8)
  bool hasActivatedAccount = false;
  @HiveField(9)
  String filePath;

  CreateAccountBody(
      {this.email,
      this.name,
      this.phone,
      this.personalIdentificationNumber,
      this.kraCertificate,
      this.password,
      this.passwordConfirm,
      this.hasFinishedRegistration,
      this.hasActivatedAccount,
      this.filePath});

  CreateAccountBody.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['name'];
    phone = json['phone'];
    personalIdentificationNumber = json['personal_identification_number'];
    kraCertificate = json['kra_certificate'];
    password = json['password'];
    passwordConfirm = json['password_confirm'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['personal_identification_number'] = this.personalIdentificationNumber;
    data['kra_certificate'] = this.kraCertificate;
    data['password'] = this.password;
    data['password_confirm'] = this.passwordConfirm;
    return data;
  }
}

@HiveType(typeId: 2)
class UserData {
  @HiveField(0)
  Address address;
  @HiveField(1)
  Documents documents;
  @HiveField(2)
  String userType;
  @HiveField(3)
  bool isSuperadmin;
  @HiveField(4)
  bool isCompanyOwner;
  @HiveField(5)
  String activationCode;
  @HiveField(6)
  bool isActive;
  @HiveField(7)
  String name;
  @HiveField(8)
  String personalIdentificationNumber;
  @HiveField(9)
  String email;
  @HiveField(10)
  String phone;
  @HiveField(11)
  String id;

  UserData(
      {this.address,
      this.documents,
      this.userType,
      this.isSuperadmin,
      this.isCompanyOwner,
      this.activationCode,
      this.isActive,
      this.name,
      this.personalIdentificationNumber,
      this.email,
      this.phone,
      this.id});

  UserData.fromJson(Map<String, dynamic> json) {
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    documents = json['documents'] != null
        ? new Documents.fromJson(json['documents'])
        : null;
    userType = json['user_type'];
    isSuperadmin = json['is_superadmin'];
    isCompanyOwner = json['is_company_owner'];
    activationCode = json['activation_code'];
    isActive = json['is_active'];
    name = json['name'];
    personalIdentificationNumber = json['personal_identification_number'];
    email = json['email'];
    phone = json['phone'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.documents != null) {
      data['documents'] = this.documents.toJson();
    }
    data['user_type'] = this.userType;
    data['is_superadmin'] = this.isSuperadmin;
    data['is_company_owner'] = this.isCompanyOwner;
    data['activation_code'] = this.activationCode;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['personal_identification_number'] = this.personalIdentificationNumber;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['id'] = this.id;
    return data;
  }
}

@HiveType(typeId: 3)
class Address {
  @HiveField(0)
  String floorNumber;
  @HiveField(1)
  String shopNumber;

  Address({this.floorNumber, this.shopNumber});

  Address.fromJson(Map<String, dynamic> json) {
    floorNumber = json['floor_number'];
    shopNumber = json['shop_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['floor_number'] = this.floorNumber;
    data['shop_number'] = this.shopNumber;
    return data;
  }
}

@HiveType(typeId: 4)
class Documents {
  @HiveField(0)
  String kraCertificate;

  Documents({this.kraCertificate});

  Documents.fromJson(Map<String, dynamic> json) {
    kraCertificate = json['kra_certificate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kra_certificate'] = this.kraCertificate;
    return data;
  }
}
