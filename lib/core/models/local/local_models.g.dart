// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'local_models.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CreateAccountBodyAdapter extends TypeAdapter<CreateAccountBody> {
  @override
  final int typeId = 1;

  @override
  CreateAccountBody read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CreateAccountBody(
      email: fields[0] as String,
      name: fields[1] as String,
      phone: fields[2] as String,
      personalIdentificationNumber: fields[3] as String,
      kraCertificate: fields[4] as String,
      password: fields[5] as String,
      passwordConfirm: fields[6] as String,
      hasFinishedRegistration: fields[7] as bool,
      hasActivatedAccount: fields[8] as bool,
      filePath: fields[9] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CreateAccountBody obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.email)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.phone)
      ..writeByte(3)
      ..write(obj.personalIdentificationNumber)
      ..writeByte(4)
      ..write(obj.kraCertificate)
      ..writeByte(5)
      ..write(obj.password)
      ..writeByte(6)
      ..write(obj.passwordConfirm)
      ..writeByte(7)
      ..write(obj.hasFinishedRegistration)
      ..writeByte(8)
      ..write(obj.hasActivatedAccount)
      ..writeByte(9)
      ..write(obj.filePath);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CreateAccountBodyAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class UserDataAdapter extends TypeAdapter<UserData> {
  @override
  final int typeId = 2;

  @override
  UserData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserData(
      address: fields[0] as Address,
      documents: fields[1] as Documents,
      userType: fields[2] as String,
      isSuperadmin: fields[3] as bool,
      isCompanyOwner: fields[4] as bool,
      activationCode: fields[5] as String,
      isActive: fields[6] as bool,
      name: fields[7] as String,
      personalIdentificationNumber: fields[8] as String,
      email: fields[9] as String,
      phone: fields[10] as String,
      id: fields[11] as String,
    );
  }

  @override
  void write(BinaryWriter writer, UserData obj) {
    writer
      ..writeByte(12)
      ..writeByte(0)
      ..write(obj.address)
      ..writeByte(1)
      ..write(obj.documents)
      ..writeByte(2)
      ..write(obj.userType)
      ..writeByte(3)
      ..write(obj.isSuperadmin)
      ..writeByte(4)
      ..write(obj.isCompanyOwner)
      ..writeByte(5)
      ..write(obj.activationCode)
      ..writeByte(6)
      ..write(obj.isActive)
      ..writeByte(7)
      ..write(obj.name)
      ..writeByte(8)
      ..write(obj.personalIdentificationNumber)
      ..writeByte(9)
      ..write(obj.email)
      ..writeByte(10)
      ..write(obj.phone)
      ..writeByte(11)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AddressAdapter extends TypeAdapter<Address> {
  @override
  final int typeId = 3;

  @override
  Address read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Address(
      floorNumber: fields[0] as String,
      shopNumber: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Address obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.floorNumber)
      ..writeByte(1)
      ..write(obj.shopNumber);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddressAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class DocumentsAdapter extends TypeAdapter<Documents> {
  @override
  final int typeId = 4;

  @override
  Documents read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Documents(
      kraCertificate: fields[0] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Documents obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.kraCertificate);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DocumentsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
