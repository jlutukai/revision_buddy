class CreateBrandResponse {
  String status;
  Brand brand;
  String message;

  CreateBrandResponse({this.status, this.brand, this.message});

  CreateBrandResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    brand = json['brand'] != null ? new Brand.fromJson(json['brand']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.brand != null) {
      data['brand'] = this.brand.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Brand {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String description;
  String id;

  Brand(
      {this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.description,
      this.id});

  Brand.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    description = json['description'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['description'] = this.description;
    data['id'] = this.id;
    return data;
  }
}
