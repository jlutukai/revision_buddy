class CreateVariationsResponse {
  String status;
  Variation variation;
  String message;

  CreateVariationsResponse({this.status, this.variation, this.message});

  CreateVariationsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    variation = json['variation'] != null
        ? new Variation.fromJson(json['variation'])
        : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.variation != null) {
      data['variation'] = this.variation.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Variation {
  List<String> values;
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String id;

  Variation(
      {this.values,
      this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.id});

  Variation.fromJson(Map<String, dynamic> json) {
    values = json['values'].cast<String>();
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['values'] = this.values;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}
