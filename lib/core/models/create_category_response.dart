class CreateCategoryResponse {
  String status;
  Category category;
  String message;

  CreateCategoryResponse({this.status, this.category, this.message});

  CreateCategoryResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Category {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String description;
  String hsCode;
  String code;
  String id;
  String label;

  Category(
      {this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.description,
      this.hsCode,
      this.code,
      this.id,
      this.label});

  Category.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    description = json['description'];
    hsCode = json['hs_code'];
    code = json['code'];
    id = json['id'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['description'] = this.description;
    data['hs_code'] = this.hsCode;
    data['code'] = this.code;
    data['id'] = this.id;
    data['label'] = this.label;
    return data;
  }
}
