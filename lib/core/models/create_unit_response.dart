class CreateUnitsResponse {
  String status;
  Unit unit;
  String message;

  CreateUnitsResponse({this.status, this.unit, this.message});

  CreateUnitsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    unit = json['unit'] != null ? new Unit.fromJson(json['unit']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.unit != null) {
      data['unit'] = this.unit.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Unit {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String shortName;
  String id;

  Unit(
      {this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.shortName,
      this.id});

  Unit.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    shortName = json['short_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['short_name'] = this.shortName;
    data['id'] = this.id;
    return data;
  }
}
