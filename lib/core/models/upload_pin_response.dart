class UploadPINResponse {
  String status;
  String url;
  Doc doc;
  String message;

  UploadPINResponse({this.status, this.url, this.doc, this.message});

  UploadPINResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    url = json['url'];
    doc = json['doc'] != null ? new Doc.fromJson(json['doc']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['url'] = this.url;
    if (this.doc != null) {
      data['doc'] = this.doc.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Doc {
  String personalIdentificationNumber;
  String certificateDate;
  String email;
  String taxPayerName;
  bool isValid;

  Doc(
      {this.personalIdentificationNumber,
      this.certificateDate,
      this.email,
      this.taxPayerName,
      this.isValid});

  Doc.fromJson(Map<String, dynamic> json) {
    personalIdentificationNumber = json['personal_identification_number'];
    certificateDate = json['certificate_date'];
    email = json['email'];
    taxPayerName = json['tax_payer_name'];
    isValid = json['is_valid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['personal_identification_number'] = this.personalIdentificationNumber;
    data['certificate_date'] = this.certificateDate;
    data['email'] = this.email;
    data['tax_payer_name'] = this.taxPayerName;
    data['is_valid'] = this.isValid;
    return data;
  }
}
