import 'local/local_models.dart';

class LoginResponse {
  UserData user;
  String status;
  String token;
  String cookie;
  String message;

  LoginResponse(
      {this.user, this.status, this.token, this.cookie, this.message});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    status = json['status'];
    token = json['token'];
    cookie = json['cookie'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['status'] = this.status;
    data['token'] = this.token;
    data['cookie'] = this.cookie;
    data['message'] = this.message;
    return data;
  }
}
