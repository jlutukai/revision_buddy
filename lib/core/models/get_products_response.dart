class GetProductsResponse {
  String status;
  Page page;
  int count;
  List<Products> products;

  GetProductsResponse({this.status, this.page, this.count, this.products});

  GetProductsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class Products {
  List<String> image;
  String barcodeType;
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  Unit unit;
  Brand brand;
  Category category;
  String sku;
  String alertQuantity;
  bool enableSerialNumber;
  String enableStock;
  String productType;
  String description;
  String taxType;
  List<Variations> variations;
  String id;

  Products(
      {this.image,
      this.barcodeType,
      this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.unit,
      this.brand,
      this.category,
      this.sku,
      this.alertQuantity,
      this.enableSerialNumber,
      this.enableStock,
      this.productType,
      this.description,
      this.taxType,
      this.variations,
      this.id});

  Products.fromJson(Map<String, dynamic> json) {
    image = json['image'].cast<String>();
    barcodeType = json['barcode_type'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    unit = json['unit'] != null ? new Unit.fromJson(json['unit']) : null;
    brand = json['brand'] != null ? new Brand.fromJson(json['brand']) : null;
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
    sku = json['sku'];
    alertQuantity = json['alert_quantity'];
    enableSerialNumber = json['enable_serial_number'];
    enableStock = json['enable_stock'];
    productType = json['product_type'];
    description = json['description'];
    taxType = json['tax_type'];
    if (json['variations'] != null) {
      variations = new List<Variations>();
      json['variations'].forEach((v) {
        variations.add(new Variations.fromJson(v));
      });
    }
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['barcode_type'] = this.barcodeType;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    if (this.unit != null) {
      data['unit'] = this.unit.toJson();
    }
    if (this.brand != null) {
      data['brand'] = this.brand.toJson();
    }
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    data['sku'] = this.sku;
    data['alert_quantity'] = this.alertQuantity;
    data['enable_serial_number'] = this.enableSerialNumber;
    data['enable_stock'] = this.enableStock;
    data['product_type'] = this.productType;
    data['description'] = this.description;
    data['tax_type'] = this.taxType;
    if (this.variations != null) {
      data['variations'] = this.variations.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    return data;
  }
}

class Unit {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String shortName;
  String id;

  Unit(
      {this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.shortName,
      this.id});

  Unit.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    shortName = json['short_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['short_name'] = this.shortName;
    data['id'] = this.id;
    return data;
  }
}

class Brand {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String description;
  String id;

  Brand(
      {this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.description,
      this.id});

  Brand.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    description = json['description'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['description'] = this.description;
    data['id'] = this.id;
    return data;
  }
}

class Category {
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String description;
  HsCode hsCode;
  String code;
  String id;
  String label;

  Category(
      {this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.description,
      this.hsCode,
      this.code,
      this.id,
      this.label});

  Category.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    description = json['description'];
    hsCode =
        json['hs_code'] != null ? new HsCode.fromJson(json['hs_code']) : null;
    code = json['code'];
    id = json['id'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['description'] = this.description;
    if (this.hsCode != null) {
      data['hs_code'] = this.hsCode.toJson();
    }
    data['code'] = this.code;
    data['id'] = this.id;
    data['label'] = this.label;
    return data;
  }
}

class HsCode {
  int quantity;
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String units;
  String description;
  String code;
  String hsType;
  String id;

  HsCode(
      {this.quantity,
      this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.units,
      this.description,
      this.code,
      this.hsType,
      this.id});

  HsCode.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    units = json['units'];
    description = json['description'];
    code = json['code'];
    hsType = json['hs_type'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quantity'] = this.quantity;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['units'] = this.units;
    data['description'] = this.description;
    data['code'] = this.code;
    data['hs_type'] = this.hsType;
    data['id'] = this.id;
    return data;
  }
}

class Variations {
  String sId;
  ProductVariation productVariation;

  Variations({this.sId, this.productVariation});

  Variations.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    productVariation = json['product_variation'] != null
        ? new ProductVariation.fromJson(json['product_variation'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.productVariation != null) {
      data['product_variation'] = this.productVariation.toJson();
    }
    return data;
  }
}

class ProductVariation {
  Null stockQuantity;
  int margin;
  int defaultPurchasePrice;
  int defaultSellingPrice;
  int sellingPriceIncTax;
  String createdAt;
  bool isActive;
  bool isEditable;
  String name;
  String subSku;
  String id;

  ProductVariation(
      {this.stockQuantity,
      this.margin,
      this.defaultPurchasePrice,
      this.defaultSellingPrice,
      this.sellingPriceIncTax,
      this.createdAt,
      this.isActive,
      this.isEditable,
      this.name,
      this.subSku,
      this.id});

  ProductVariation.fromJson(Map<String, dynamic> json) {
    stockQuantity = json['stock_quantity'];
    margin = json['margin'];
    defaultPurchasePrice = json['default_purchase_price'];
    defaultSellingPrice = json['default_selling_price'];
    sellingPriceIncTax = json['selling_price_inc_tax'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isEditable = json['is_editable'];
    name = json['name'];
    subSku = json['sub_sku'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['stock_quantity'] = this.stockQuantity;
    data['margin'] = this.margin;
    data['default_purchase_price'] = this.defaultPurchasePrice;
    data['default_selling_price'] = this.defaultSellingPrice;
    data['selling_price_inc_tax'] = this.sellingPriceIncTax;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_editable'] = this.isEditable;
    data['name'] = this.name;
    data['sub_sku'] = this.subSku;
    data['id'] = this.id;
    return data;
  }
}
