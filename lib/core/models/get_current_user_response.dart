class GetCurrentUserResponse {
  User user;
  String status;

  GetCurrentUserResponse({this.user, this.status});

  GetCurrentUserResponse.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class User {
  String id;
  String name;
  String personalIdentificationNumber;
  String phone;
  String userType;
  bool isSuperadmin;
  Address address;
  // Locations locations;
  Documents documents;
  // List<Null> securityQuestions;
  String email;
  bool isActive;
  int iat;

  User(
      {this.id,
      this.name,
      this.personalIdentificationNumber,
      this.phone,
      this.userType,
      this.isSuperadmin,
      this.address,
      // this.locations,
      this.documents,
      // this.securityQuestions,
      this.email,
      this.isActive,
      this.iat});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    personalIdentificationNumber = json['personal_identification_number'];
    phone = json['phone'];
    userType = json['user_type'];
    isSuperadmin = json['is_superadmin'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    // locations = json['locations'] != null ? new Locations.fromJson(json['locations']) : null;
    documents = json['documents'] != null
        ? new Documents.fromJson(json['documents'])
        : null;
    // if (json['security_questions'] != null) {
    //   securityQuestions = new List<Null>();
    //   json['security_questions'].forEach((v) { securityQuestions.add(new Null.fromJson(v)); });
    // }
    email = json['email'];
    isActive = json['is_active'];
    iat = json['iat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['personal_identification_number'] = this.personalIdentificationNumber;
    data['phone'] = this.phone;
    data['user_type'] = this.userType;
    data['is_superadmin'] = this.isSuperadmin;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    // if (this.locations != null) {
    //   data['locations'] = this.locations.toJson();
    // }
    if (this.documents != null) {
      data['documents'] = this.documents.toJson();
    }
    // if (this.securityQuestions != null) {
    //   data['security_questions'] = this.securityQuestions.map((v) => v.toJson()).toList();
    // }
    data['email'] = this.email;
    data['is_active'] = this.isActive;
    data['iat'] = this.iat;
    return data;
  }
}

class Address {
  String floorNumber;
  String shopNumber;

  Address({this.floorNumber, this.shopNumber});

  Address.fromJson(Map<String, dynamic> json) {
    floorNumber = json['floor_number'];
    shopNumber = json['shop_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['floor_number'] = this.floorNumber;
    data['shop_number'] = this.shopNumber;
    return data;
  }
}

// class Locations {
//   Gps gps;
//
//   Locations({this.gps});
//
//   Locations.fromJson(Map<String, dynamic> json) {
//     gps = json['gps'] != null ? new Gps.fromJson(json['gps']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.gps != null) {
//       data['gps'] = this.gps.toJson();
//     }
//     return data;
//   }
// }

// class Gps {
//
//
//   Gps({});
//
// Gps.fromJson(Map<String, dynamic> json) {
// }
//
// Map<String, dynamic> toJson() {
//   final Map<String, dynamic> data = new Map<String, dynamic>();
//   return data;
// }
// }

class Documents {
  String kraCertificate;
  // List<Null> otherDocs;

  Documents({
    this.kraCertificate,
    // this.otherDocs
  });

  Documents.fromJson(Map<String, dynamic> json) {
    kraCertificate = json['kra_certificate'];
    if (json['other_docs'] != null) {
      // otherDocs = new List<Null>();
      // json['other_docs'].forEach((v) { otherDocs.add(new Null.fromJson(v)); });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kra_certificate'] = this.kraCertificate;
    // if (this.otherDocs != null) {
    //   data['other_docs'] = this.otherDocs.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}
