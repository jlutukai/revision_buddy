import 'package:revision_plus/core/enums/view_state.dart';
import 'package:revision_plus/core/models/login_response.dart';
import 'package:revision_plus/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class LoginModel extends BaseModel {
  Api _api = locator<Api>();

  Future<LoginResponse> loginUser(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      // var r = await _api.login(data);
      setState(ViewState.Idle);
      // return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
