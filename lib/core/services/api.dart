import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:revision_plus/utils/logging_interceptor.dart';

class Api {
  static const baseUrl = 'https://smartetr.xyz/api/v1';
  Dio _dio;
  String firebaseId = "";

  Api() {
    BaseOptions options =
        BaseOptions(receiveTimeout: 50000, connectTimeout: 50000);
    _dio = Dio(options)
      ..interceptors.add(LoggingInterceptor())
      ..interceptors
          .add(DioCacheManager(CacheConfig(baseUrl: baseUrl)).interceptor)
      // ..httpClientAdapter = Http2Adapter(
      //   ConnectionManager(
      //     idleTimeout: 50000,
      //     onClientCreate: (_, config) => config.onBadCertificate = (_) => true,
      //   ),
      // )
      ..options.headers["content-type"] = "application/json";
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) {
        final isValidHost = ["https://smartetr.xyz"]
            .contains(host); // <-- allow only hosts in array
        return true;
      });
  }
}

//https://www.getpostman.com/collections/095b8f70005975c81589
