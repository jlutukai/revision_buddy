import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:revision_plus/ui/pages/home_page.dart';
import 'package:revision_plus/ui/pages/login_page.dart';
import 'package:revision_plus/ui/pages/registration_page.dart';
import 'package:revision_plus/ui/pages/reset_password.dart';
import 'package:revision_plus/ui/pages/splash_page.dart';
import 'package:revision_plus/utils/constants.dart';

import 'locator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Directory document = await getApplicationDocumentsDirectory();
  Hive..init(document.path);
  await Hive.openBox<String>("account_info");
  await Hive.openBox("current_user");
  await Hive.openBox("create_user");
  setUpLocator();
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Revision Plus',
      theme: ThemeData(
        textTheme: GoogleFonts.spartanTextTheme(
          Theme.of(context).textTheme,
        ),
        primaryColor: fromHex(yellow),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        '/': (context) => SplashScreen(),
        LoginPage.tag: (context) => LoginPage(),
        RegistrationPage.tag: (context) => RegistrationPage(),
        HomePage.tag: (context) => HomePage(),
        ResetPassword.tag: (context) => ResetPassword(),
      },
    );
  }
}
