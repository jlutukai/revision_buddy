import 'package:get_it/get_it.dart';

import 'core/services/api.dart';
import 'core/viewmodels/login_model.dart';
import 'core/viewmodels/otp_model.dart';
import 'core/viewmodels/request_password_reset_model.dart';

GetIt locator = GetIt.instance;

void setUpLocator() {
  locator.registerLazySingleton(() => Api());

  // locator.registerFactory(() => AccountTypeModel());
  // locator.registerFactory(() => AuthModel());
  locator.registerFactory(() => OTPModel());
  locator.registerFactory(() => RequestPasswordResetModel());
  locator.registerFactory(() => LoginModel());
  // locator.registerFactory(() => ProfileModel());
  // locator.registerFactory(() => CategoriesModel());
  // locator.registerFactory(() => BrandModel());
  // locator.registerFactory(() => UnitModel());
  // locator.registerFactory(() => VariationModel());
}
