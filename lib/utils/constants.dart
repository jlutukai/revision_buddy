import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

Widget loader() {
  return Center(
    child: CircularProgressIndicator(
      backgroundColor: fromHex(dark_yellow),
      valueColor: new AlwaysStoppedAnimation<Color>(
        fromHex(dark_blue),
      ),
    ),
  );
}

var df = DateFormat("dd/MM/yyyy");
var tf = DateFormat("HH:mm");

const String image =
    "https://images.unsplash.com/photo-1521572267360-ee0c2909d518?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NjEzfHxwcm9maWxlJTIwcGljdHVyZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60";

const String dark_yellow = "#EDB423";
const String yellow = "#FFC226";
const String light_yellow = "#FFCF54";
const String lite_yellow = "#F9E4B0";
const String dark_blue = "#2C3051";
const String blue = "#53588A";
const String light_blue = "#9BA5FF";
const String lite_blue = "#D8DCFF";
const String grey = '#707070';
const String background = "#EDEDEF";
const String lorem =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem unknown printer took a galley of type and scrambled it to make a type specimen book.";

showToast(String msg) {
  Fluttertoast.showToast(msg: msg);
}

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
}

getBorder() {
  return UnderlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: Colors.white),
  );
}

getEnabledBorder() {
  return UnderlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: fromHex(dark_yellow), width: 3),
  );
}

setToken(String s) {
  Box<String> accountInfo = Hive.box<String>("account_info");
  accountInfo.put("token", s);
}

String getToken() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  return accountInfo.get("token");
}

deleteToken() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  return accountInfo.delete("token");
}

// setCurrentUser(UserData s) {
//   Box user = Hive.box("current_user");
//   user.put("user", s);
// }
//
// UserData getCurrentUser() {
//   Box user = Hive.box("current_user");
//   return user.get("user");
// }

deleteUser() {
  Box user = Hive.box("current_user");
  return user.delete("user");
}

// setAddUserData(CreateAccountBody s) {
//   Box user = Hive.box("create_user");
//   user.put("user", s);
// }
//
// CreateAccountBody getAddUserData() {
//   Box user = Hive.box("create_user");
//   return user.get("user");
// }

deleteAddUserData() {
  Box user = Hive.box("create_user");
  return user.delete("user");
}

deleteEveryThing() {
  deleteAddUserData();
  deleteToken();
  deleteUser();
}
