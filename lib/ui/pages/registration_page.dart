import 'package:flutter/material.dart';
import 'package:revision_plus/ui/widgets/registration/account_details.dart';
import 'package:revision_plus/ui/widgets/registration/auth_info.dart';

import '../../utils/constants.dart';
import 'login_page.dart';

class RegistrationPage extends StatefulWidget {
  static const tag = 'registration';
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  int _currentIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    deleteAddUserData();
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void onDataChange(int index) {
    print("pressed");
    setState(() {
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 300), curve: Curves.easeInToLinear);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: fromHex(background),
          resizeToAvoidBottomInset: true,
          body: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    decoration: BoxDecoration(color: fromHex(yellow)),
                  ),
                  Positioned(
                    right: -200,
                    top: -50,
                    child: Container(
                      width: MediaQuery.of(context).size.height * 0.50,
                      height: MediaQuery.of(context).size.height * 0.50,
                      decoration: new BoxDecoration(
                        color: fromHex(light_yellow),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Join us today",
                                  style: TextStyle(
                                      color: fromHex(blue),
                                      fontWeight: FontWeight.w900,
                                      fontSize: 24),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Register to continue...",
                                  style: TextStyle(
                                      color: fromHex(blue),
                                      fontWeight: FontWeight.w100,
                                      fontSize: 12),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  onPageChanged: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                  children: <Widget>[
                    AccountDetails(onDataChange),
                    AuthInfo(onDataChange)
                    // AccountType(onDataChange),
                    // AuthInfo(onDataChange),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 20,
          top: 55,
          child: FloatingActionButton(
            foregroundColor: Colors.white,
            backgroundColor: fromHex(dark_yellow),
            onPressed: () {
              if (_currentIndex == 0) {
                Navigator.pushNamedAndRemoveUntil(
                    context, LoginPage.tag, (Route<dynamic> route) => false);
              }
              if (_currentIndex == 1) {
                onDataChange(0);
              }
            },
            mini: true,
            tooltip: "go back",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(blue),
            ),
          ),
        ),
      ],
    );
  }
}
