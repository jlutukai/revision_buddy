import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:revision_plus/core/enums/view_state.dart';
import 'package:revision_plus/core/viewmodels/login_model.dart';
import 'package:revision_plus/ui/pages/registration_page.dart';
import 'package:revision_plus/ui/pages/reset_password.dart';
import 'package:revision_plus/utils/constants.dart';

import 'base_view.dart';
import 'home_page.dart';

class LoginPage extends StatefulWidget {
  static const tag = 'login';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isHidden = true;
  String pass = "";
  TextEditingController _userName = TextEditingController();
  TextEditingController _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginModel>(
      builder: (context, model, child) => Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  decoration: BoxDecoration(color: fromHex(yellow)),
                ),
                Positioned(
                  right: -200,
                  top: -50,
                  child: Container(
                    width: MediaQuery.of(context).size.height * 0.50,
                    height: MediaQuery.of(context).size.height * 0.50,
                    decoration: new BoxDecoration(
                      color: fromHex(light_yellow),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Welcome Back",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontWeight: FontWeight.w900,
                                    fontSize: 24),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Login to continue...",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontWeight: FontWeight.w100,
                                    fontSize: 12),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Container(
                child: _loginDetails(context, model),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  _loginDetails(BuildContext context, LoginModel model) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _formKey,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.75,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    children: [
                      ClipOval(
                        child: SvgPicture.asset(
                          "assets/logo.svg",
                          height: 100,
                          width: 100,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Revision Plus',
                        style: TextStyle(
                            color: fromHex(blue), fontWeight: FontWeight.w800),
                      ),
                      Text('Revision Made Easy',
                          style: GoogleFonts.pacifico(
                            textStyle: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w100,
                                color: fromHex(grey)),
                          ))
                    ],
                  ),
                ),
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    child: PhysicalModel(
                      elevation: 5,
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      child: TextFormField(
                        controller: _userName,
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.black),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please Enter email";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          isDense: true,
                          hintText: "johndoe@example.com",
                          labelText: "Email",
                          labelStyle: TextStyle(color: fromHex(grey)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: getBorder(),
                          enabledBorder: getBorder(),
                          focusedBorder: getEnabledBorder(),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    padding: EdgeInsets.only(top: 15),
                    child: PhysicalModel(
                      elevation: 5,
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      child: TextFormField(
                        controller: _password,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: _isHidden,
                        style: TextStyle(color: Colors.black),
                        onChanged: (val) {
                          pass = val;
                          setState(() {});
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please your password";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          isDense: true,
                          labelText: "Password",
                          labelStyle: TextStyle(color: fromHex(grey)),
                          hintText: "Your password",
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: getBorder(),
                          enabledBorder: getBorder(),
                          focusedBorder: getEnabledBorder(),
                          suffixIcon: pass.isNotEmpty
                              ? IconButton(
                                  onPressed: _toggleVisibility,
                                  icon: _isHidden
                                      ? Icon(
                                          Icons.visibility_off,
                                          color: Colors.grey[400],
                                        )
                                      : Icon(
                                          Icons.visibility,
                                          color: fromHex(dark_yellow),
                                        ),
                                )
                              : null,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, top: 25),
                    child: Row(
                      children: [
                        Text('Forgot Password ? '),
                        GestureDetector(
                          onTap: () {
                            if (model.state == ViewState.Idle) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  ResetPassword.tag,
                                  (Route<dynamic> route) => false);
                            }
                          },
                          child: Text(
                            'Reset Password',
                            style: TextStyle(
                              color: fromHex(dark_yellow),
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        model.state == ViewState.Idle
                            ? RaisedButton(
                                elevation: 5.0,
                                onPressed: () async {
                                  // if (_formKey.currentState.validate()) {
                                  //   Map<String, String> data = {
                                  //     "personal_identification_number":
                                  //         "${_userName.text}",
                                  //     "password": "${_password.text}"
                                  //   };
                                  //   try {
                                  //     var r = await model.loginUser(data);
                                  //     showToast(r.message);
                                  // setCurrentUser(r.user);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context,
                                      HomePage.tag,
                                      (Route<dynamic> route) => false);
                                  // } catch (e) {
                                  //   try {
                                  //     Map<String, dynamic> error =
                                  //         jsonDecode(e.toString());
                                  //     showToast(
                                  //         error['errors'][0]['message']);
                                  //   } catch (a) {}
                                  // }
                                  // }
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: fromHex(dark_yellow),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0),
                                  child: Center(
                                    child: Row(
                                      children: [
                                        Text(
                                          'Login',
                                          style: TextStyle(
                                              color: fromHex(blue),
                                              fontSize: 18),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios,
                                          size: 16,
                                          color: fromHex(blue),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : loader(),
                      ],
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Don't have an account? "),
                        GestureDetector(
                          onTap: () {
                            if (model.state == ViewState.Idle) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  RegistrationPage.tag,
                                  (Route<dynamic> route) => false);
                            }
                          },
                          child: Text(
                            'Create Account',
                            style: TextStyle(
                              color: fromHex(dark_yellow),
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
