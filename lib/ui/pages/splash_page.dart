import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:revision_plus/utils/constants.dart';

import 'login_page.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(Duration(seconds: 5), () {
      // if (getToken() != null && getToken().isNotEmpty) {
      //   Navigator.pushNamedAndRemoveUntil(
      //       context, HomePage.tag, (Route<dynamic> route) => false);
      // } else {
      Navigator.pushNamedAndRemoveUntil(
          context, LoginPage.tag, (Route<dynamic> route) => false);
      // }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(yellow),
      body: Column(
        children: [
          Expanded(
              child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Text(
                  //   'Welcome',
                  //   style: TextStyle(fontSize: 24, fontWeight: FontWeight.w800),
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  ClipOval(
                    child: SvgPicture.asset(
                      "assets/logo.svg",
                      height: 200,
                      width: 200,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Revision Plus',
                    style: TextStyle(
                        color: fromHex(blue), fontWeight: FontWeight.w800),
                  )
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
