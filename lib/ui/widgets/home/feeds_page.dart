import 'package:flutter/material.dart';
import 'package:revision_plus/utils/constants.dart';

class FeedsPage extends StatefulWidget {
  @override
  _FeedsPageState createState() => _FeedsPageState();
}

class _FeedsPageState extends State<FeedsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(background),
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.25,
                decoration: BoxDecoration(color: fromHex(yellow)),
              ),
              Positioned(
                right: -200,
                top: -50,
                child: Container(
                  width: MediaQuery.of(context).size.height * 0.50,
                  height: MediaQuery.of(context).size.height * 0.50,
                  decoration: new BoxDecoration(
                    color: fromHex(light_yellow),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, bottom: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text(
                              "Welcome John Doe",
                              style: TextStyle(
                                  color: fromHex(blue),
                                  fontWeight: FontWeight.w900,
                                  fontSize: 24),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "Browse through available content...",
                              style: TextStyle(
                                  color: fromHex(blue),
                                  fontWeight: FontWeight.w100,
                                  fontSize: 12),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }
}
