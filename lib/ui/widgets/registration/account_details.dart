import 'package:flutter/material.dart';
import 'package:revision_plus/ui/pages/login_page.dart';
import 'package:revision_plus/utils/constants.dart';

class AccountDetails extends StatefulWidget {
  final void Function(int index) onDataChange;
  AccountDetails(this.onDataChange);

  @override
  _AccountDetailsState createState() => _AccountDetailsState();
}

class _AccountDetailsState extends State<AccountDetails> {
  TextEditingController _fname = TextEditingController();
  TextEditingController _oname = TextEditingController();
  TextEditingController _email = TextEditingController();
  String _accountType = "";
  TextEditingController _phone = TextEditingController();
  int _radioValue = -1;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(background),
      body: Column(
        children: [
          SizedBox(
            height: 35,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Account Info',
                style: TextStyle(
                    color: fromHex(blue),
                    fontSize: 26,
                    fontWeight: FontWeight.w100),
              ),
            ],
          ),
          SizedBox(
            height: 35,
          ),
          Expanded(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Form(
                key: _formKey,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.61,
                  child: Column(
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 25),
                              child: PhysicalModel(
                                elevation: 5,
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                child: TextFormField(
                                  controller: _fname,
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.words,
                                  style: TextStyle(color: Colors.black),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please Enter Your first name";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    isDense: true,
                                    hintText: "John",
                                    labelText: "First Name",
                                    labelStyle: TextStyle(color: fromHex(grey)),
                                    hintStyle: TextStyle(
                                      color: Colors.blueGrey[400],
                                    ),
                                    border: getBorder(),
                                    enabledBorder: getBorder(),
                                    focusedBorder: getEnabledBorder(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 25),
                              child: PhysicalModel(
                                elevation: 5,
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                child: TextFormField(
                                  controller: _oname,
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.words,
                                  style: TextStyle(color: Colors.black),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please Enter other names";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    isDense: true,
                                    hintText: "Doe",
                                    labelText: "Other Names",
                                    labelStyle: TextStyle(color: fromHex(grey)),
                                    hintStyle: TextStyle(
                                      color: Colors.blueGrey[400],
                                    ),
                                    border: getBorder(),
                                    enabledBorder: getBorder(),
                                    focusedBorder: getEnabledBorder(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 25),
                              child: PhysicalModel(
                                elevation: 5,
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                child: TextFormField(
                                  controller: _email,
                                  keyboardType: TextInputType.emailAddress,
                                  style: TextStyle(color: Colors.black),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please Enter email address";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    isDense: true,
                                    hintText: "johndoe@email.com",
                                    labelText: "Email Address",
                                    labelStyle: TextStyle(color: fromHex(grey)),
                                    hintStyle: TextStyle(
                                      color: Colors.blueGrey[400],
                                    ),
                                    border: getBorder(),
                                    enabledBorder: getBorder(),
                                    focusedBorder: getEnabledBorder(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 25),
                              child: PhysicalModel(
                                elevation: 5,
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                child: TextFormField(
                                  controller: _phone,
                                  keyboardType: TextInputType.phone,
                                  style: TextStyle(color: Colors.black),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please Enter phone number";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    isDense: true,
                                    hintText: "0700 000 000",
                                    labelText: "Phone (m-pesa)",
                                    labelStyle: TextStyle(color: fromHex(grey)),
                                    hintStyle: TextStyle(
                                      color: Colors.blueGrey[400],
                                    ),
                                    border: getBorder(),
                                    enabledBorder: getBorder(),
                                    focusedBorder: getEnabledBorder(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 25),
                              child: PhysicalModel(
                                elevation: 5,
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      _accountType.isNotEmpty
                                          ? Row(
                                              children: [
                                                Text(
                                                  "I am a ",
                                                  style: TextStyle(
                                                      color: fromHex(grey)),
                                                ),
                                                Text(
                                                  "$_accountType",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w800),
                                                ),
                                              ],
                                            )
                                          : Container(),
                                      Theme(
                                        data: ThemeData(
                                            accentColor: fromHex(grey)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Radio(
                                              value: 0,
                                              activeColor: fromHex(dark_yellow),
                                              groupValue: _radioValue,
                                              onChanged:
                                                  _handleRadioValueChange,
                                            ),
                                            Text(
                                              'Student',
                                              style: TextStyle(
                                                fontSize: 12.0,
                                                color: fromHex(blue),
                                              ),
                                            ),
                                            Radio(
                                              value: 1,
                                              groupValue: _radioValue,
                                              activeColor: fromHex(dark_yellow),
                                              onChanged:
                                                  _handleRadioValueChange,
                                            ),
                                            Text(
                                              'Parent',
                                              style: TextStyle(
                                                fontSize: 12.0,
                                                color: fromHex(blue),
                                              ),
                                            ),
                                            Radio(
                                              value: 2,
                                              groupValue: _radioValue,
                                              activeColor: fromHex(dark_yellow),
                                              onChanged:
                                                  _handleRadioValueChange,
                                            ),
                                            Text(
                                              'Teacher',
                                              style: TextStyle(
                                                fontSize: 12.0,
                                                color: fromHex(blue),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                // model.state == ViewState.Idle
                                //     ?
                                RaisedButton(
                                  elevation: 5.0,
                                  onPressed: () async {
                                    widget.onDataChange(1);
                                    // if (_formKey.currentState.validate()) {
                                    //   // Map<String, String> data = {
                                    //   //   "personal_identification_number":
                                    //   //   "${_userName.text}",
                                    //   //   "password": "${_password.text}"
                                    //   // };
                                    //   // try {
                                    //   //   var r = await model.loginUser(data);
                                    //   //   showToast(r.message);
                                    //   //   // setCurrentUser(r.user);
                                    //   //   Navigator.pushNamedAndRemoveUntil(
                                    //   //       context,
                                    //   //       HomePage.tag,
                                    //   //           (Route<dynamic> route) => false);
                                    //   // } catch (e) {
                                    //   //   try {
                                    //   //     Map<String, dynamic> error =
                                    //   //     jsonDecode(e.toString());
                                    //   //     showToast(
                                    //   //         error['errors'][0]['message']);
                                    //   //   } catch (a) {}
                                    //   // }
                                    // }
                                  },
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  color: fromHex(dark_yellow),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10.0),
                                    child: Center(
                                      child: Row(
                                        children: [
                                          Text(
                                            'Next',
                                            style: TextStyle(
                                                color: fromHex(blue),
                                                fontSize: 18),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Icon(
                                            Icons.arrow_forward_ios,
                                            size: 16,
                                            color: fromHex(blue),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                                // : loader(),
                              ],
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Already have an account? "),
                                GestureDetector(
                                  onTap: () {
                                    // if (model.state == ViewState.Idle) {
                                    Navigator.pushNamedAndRemoveUntil(
                                        context,
                                        LoginPage.tag,
                                        (Route<dynamic> route) => false);
                                    // }
                                  },
                                  child: Text(
                                    'Log In',
                                    style: TextStyle(
                                      color: fromHex(dark_yellow),
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _handleRadioValueChange(int value) {
    print(value);
    setState(() {
      _radioValue = value;
    });
    if (value == 0) {
      setState(() {
        _accountType = "Student";
      });
    }
    if (value == 1) {
      setState(() {
        _accountType = "Parent / Guardian";
      });
    }
    if (value == 2) {
      setState(() {
        _accountType = "Teacher / Tutor";
      });
    }
  }
}
